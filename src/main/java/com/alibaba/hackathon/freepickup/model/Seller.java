package com.alibaba.hackathon.freepickup.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(
        name = "seller",
        indexes = {
//                @Index(name = "uniq_field1_field2", columnList = "field1, field2", unique = true),
//                @Index(name = "idx_field3_field4", columnList = "field3, field4")
        }
)
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@EqualsAndHashCode
@Setter
@Getter
@ToString
public class Seller {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "courier_id")
    private Long courierId;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "geo_lat", columnDefinition = "decimal(13,9)")
    private Float geoLat;

    @Column(name = "geo_long", columnDefinition = "decimal(13,9)")
    private Float geoLong;

    @Column(name = "total_rts")
    private Long totalRts;

}
