package com.alibaba.hackathon.freepickup.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NodeRepository extends JpaRepository<Node, Long>, JpaSpecificationExecutor<Node> {

    @Query(value = "SELECT id, (3959 * acos(cos(radians(:geoLat)) * cos(radians(geo_lat)) * cos( radians(geo_long) - radians(:geoLong)) + sin(radians(:geoLat)) * sin(radians(geo_lat)))) AS distance " +
            "FROM node " +
//            "HAVING distance < 25 " +
            "ORDER BY distance " +
            "LIMIT 0, :nRecords",
            nativeQuery = true)
    List<NearestNode> findNearestNodes(
            @Param("geoLat") Float geoLat,
            @Param("geoLong") Float geoLong,
            @Param("nRecords") long nRecords
    );

    interface NearestNode {

        Long getId();

        Float getDistance();

    }

}
