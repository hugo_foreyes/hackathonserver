package com.alibaba.hackathon.freepickup.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SellerRepository extends JpaRepository<Seller, Long>, JpaSpecificationExecutor<Seller> {

    @Query(value = "SELECT id, (3959 * acos(cos(radians(:geoLat)) * cos(radians(geo_lat)) * cos( radians(geo_long) - radians(:geoLong)) + sin(radians(:geoLat)) * sin(radians(geo_lat)))) AS distance " +
            "FROM seller " +
            "WHERE total_rts > 0  AND courier_id IS NULL " +
//            "HAVING distance < 25 " +
            "ORDER BY distance " +
            "LIMIT 0, :nRecords",
            nativeQuery = true)
    List<NearestSeller> findNearestSellers(
            @Param("geoLat") Float geoLat,
            @Param("geoLong") Float geoLong,
            @Param("nRecords") long nRecords
    );

//        @Query(value = "SELECT id, ((3959 * acos(cos(radians(:geoLat)) * cos(radians(geo_lat)) * cos( radians(geo_long) - radians(:geoLong)) + sin(radians(:geoLat)) * sin(radians(geo_lat)))) " +
//            "+ (3959 * acos(cos(radians(:nodeGeoLat)) * cos(radians(geo_lat)) * cos( radians(geo_long) - radians(:nodeGeoLong)) + sin(radians(:nodeGeoLat)) * sin(radians(geo_lat))))) AS distance " +
        @Query(value = "SELECT id, (3959 * acos(cos(radians(:geoLat)) * cos(radians(geo_lat)) * cos( radians(geo_long) - radians(:geoLong)) + sin(radians(:geoLat)) * sin(radians(geo_lat)))) AS distance, " +
            "(3959 * acos(cos(radians(:nodeGeoLat)) * cos(radians(geo_lat)) * cos( radians(geo_long) - radians(:nodeGeoLong)) + sin(radians(:nodeGeoLat)) * sin(radians(geo_lat)))) AS node_distance " +
            "FROM seller " +
            "WHERE total_rts > 0 AND courier_id IS NULL " +
//            "HAVING distance < 25 " +
            "ORDER BY node_distance, distance " +
            "LIMIT 0, :nRecords",
            nativeQuery = true)
    List<NearestSeller> findNearestSellersWithNode(
            @Param("geoLat") Float geoLat,
            @Param("geoLong") Float geoLong,
            @Param("nodeGeoLat") Float nodeGeoLat,
            @Param("nodeGeoLong") Float nodeGeoLong,
            @Param("nRecords") long nRecords
    );

    List<Seller> findByCourierId(Long courierId);

    interface NearestSeller {

        Long getId();

        Float getDistance();

        Float getNodeDistance();

    }

}
