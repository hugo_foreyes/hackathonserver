package com.alibaba.hackathon.freepickup.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(
        name = "node",
        indexes = {
//                @Index(name = "uniq_field1_field2", columnList = "field1, field2", unique = true),
//                @Index(name = "idx_field3_field4", columnList = "field3, field4")
        }
)
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@EqualsAndHashCode
@Setter(AccessLevel.PACKAGE)
@Getter
@ToString
public class Node {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private NodeType type;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "geo_lat", columnDefinition = "decimal(7,5)")
    private Float geoLat;

    @Column(name = "geo_long", columnDefinition = "decimal(7,5)")
    private Float geoLong;

    @Column(name = "cut_off_name")
    private String cutOffTime;

}
