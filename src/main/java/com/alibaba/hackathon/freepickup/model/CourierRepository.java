package com.alibaba.hackathon.freepickup.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Repository
public interface CourierRepository extends JpaRepository<Courier, Long>, JpaSpecificationExecutor<Courier> {

    @Query(value = "SELECT * FROM courier WHERE phone = :phone", nativeQuery = true)
    Courier findCourierByPhone(@Param("phone") String phone);

}
