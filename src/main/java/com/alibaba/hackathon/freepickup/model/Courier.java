package com.alibaba.hackathon.freepickup.model;

import lombok.*;

import javax.persistence.*;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Entity
@Table(
	name = "courier",
	indexes = {
		//                @Index(name = "uniq_field1_field2", columnList = "field1, field2", unique = true),
		//                @Index(name = "idx_field3_field4", columnList = "field3, field4")
	}
)
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@EqualsAndHashCode
@Setter
@Getter
@ToString
public class Courier {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "phone")
	private String phone;

	@Column(name = "status")
	private String status;

	@Column(name = "destionation_node_id")
	private Long destinationNodeId;

}
