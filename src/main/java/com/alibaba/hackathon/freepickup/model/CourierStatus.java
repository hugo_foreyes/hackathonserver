package com.alibaba.hackathon.freepickup.model;

/**
 * @author lazada team
 * @date 2022/02/23
 */
public enum CourierStatus {
	FREE,
	PICKING,
	RETURNING
}
