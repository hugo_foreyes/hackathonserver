package com.alibaba.hackathon.freepickup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreepickupApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreepickupApplication.class, args);
	}

}
