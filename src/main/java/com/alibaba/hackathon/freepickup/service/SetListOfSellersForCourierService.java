package com.alibaba.hackathon.freepickup.service;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import com.alibaba.hackathon.freepickup.model.Courier;
import com.alibaba.hackathon.freepickup.model.CourierRepository;
import com.alibaba.hackathon.freepickup.model.CourierStatus;
import com.alibaba.hackathon.freepickup.model.Seller;
import com.alibaba.hackathon.freepickup.model.SellerRepository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Service
@AllArgsConstructor
public class SetListOfSellersForCourierService {
	private SellerRepository sellerRepository;
	private CourierRepository courierRepository;

	public Response process(@NonNull Request request) {
		Courier courier = this.courierRepository.findById(request.getCourierId())
			.orElseThrow(() -> new RuntimeException("Courier does not exist with id"+request.getCourierId()));

		courier.setStatus(CourierStatus.PICKING.name());
		this.courierRepository.save(courier);

		List<Seller> sellers = this.sellerRepository.findAllById(request.getSellerIds());
		if (!CollectionUtils.isEmpty(sellers)) {
			sellers.forEach(seller -> seller.setCourierId(courier.getId()));
			this.sellerRepository.saveAll(sellers);
		}

		return Response.builder()
			.code(200L)
			.isSuccess(true)
			.data(true)
			.build();
	}

	@Builder
	@Getter
	@ToString
	public static class Request {

		@NonNull
		private Long courierId;
		@NotEmpty
		private List<Long> sellerIds;

	}

	@Builder
	@Getter
	@ToString
	public static class Response {
		private Long code;
		private boolean isSuccess;
		private boolean data;
	}
}
