package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.model.*;
import lombok.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Service
@AllArgsConstructor
public class ChangeCourierStatusService {
	private CourierRepository courierRepository;
	private SellerRepository sellerRepository;

	public Response process(@NonNull Request request) {
		Courier courier = this.courierRepository.findById(request.getCourierId()).orElseThrow(
			() -> new RuntimeException("Courier does not exist with id " + request.getCourierId()));

		if (CourierStatus.FREE.name().equals(request.getStatus())) {
			List<Seller> sellerList = this.sellerRepository.findByCourierId(request.getCourierId());
			if (!CollectionUtils.isEmpty(sellerList)) {
				sellerList.forEach(seller -> seller.setCourierId(null));
				this.sellerRepository.saveAll(sellerList);
			}
		}

		if (!Arrays.asList(CourierStatus.FREE.name(), CourierStatus.RETURNING.name(), CourierStatus.PICKING.name()).contains(request.getStatus())) {
			throw new RuntimeException("Status must be "+CourierStatus.RETURNING.name() + ", "+CourierStatus.FREE.name()+" or "+CourierStatus.PICKING.name());
		}

		courier.setStatus(request.getStatus());
		this.courierRepository.save(courier);

		return Response.builder()
			.code(200L)
			.isSuccess(true)
			.data(true)
			.build();
	}

	@Builder
	@Getter
	@ToString
	public static class Request {

		@NonNull
		private Long courierId;
		@NonNull
		private String status;

	}

	@Builder
	@Getter
	@ToString
	public static class Response {
		private Long code;
		private boolean isSuccess;
		private boolean data;
	}
}
