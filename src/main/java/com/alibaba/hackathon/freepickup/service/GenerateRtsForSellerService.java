package com.alibaba.hackathon.freepickup.service;

import java.util.List;
import java.util.Random;

import com.alibaba.hackathon.freepickup.model.Seller;
import com.alibaba.hackathon.freepickup.model.SellerRepository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Service
@AllArgsConstructor
public class GenerateRtsForSellerService {
	private SellerRepository sellerRepository;

	public Response process() {
		List<Seller> sellerList = this.sellerRepository.findAll();
		if (!CollectionUtils.isEmpty(sellerList)) {
			sellerList.forEach(seller -> seller.setTotalRts(this.randomNumber().longValue()));
			this.sellerRepository.saveAll(sellerList);
		}

		return Response.builder()
			.code(200L)
			.isSuccess(true)
			.data(true)
			.build();
	}

	private Integer randomNumber() {
		Random r = new Random();
		return r.nextInt(100) + 1;
	}

	@Builder
	@Getter
	@ToString
	public static class Response {
		private Long code;
		private boolean isSuccess;
		private boolean data;
	}
}
