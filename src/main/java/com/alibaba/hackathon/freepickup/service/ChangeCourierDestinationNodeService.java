package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.dto.ChangeCourierDestinationNodeRequest;
import com.alibaba.hackathon.freepickup.model.Courier;
import com.alibaba.hackathon.freepickup.model.CourierRepository;
import lombok.*;
import org.springframework.stereotype.Service;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Service
@RequiredArgsConstructor
public class ChangeCourierDestinationNodeService {

	private final CourierRepository courierRepository;

	public boolean process(ChangeCourierDestinationNodeRequest request) {
		Courier courier = this.courierRepository.findById(request.getCourierId()).orElseThrow(
			() -> new RuntimeException("Courier does not exist with id " + request.getCourierId()));

		courier.setDestinationNodeId(request.getDestinationNodeId());
		this.courierRepository.save(courier);

		return true;
	}

}
