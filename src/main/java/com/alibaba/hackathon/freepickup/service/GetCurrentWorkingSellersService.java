package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.dto.SellerDto;
import com.alibaba.hackathon.freepickup.model.Seller;
import com.alibaba.hackathon.freepickup.model.SellerRepository;
import lombok.*;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GetCurrentWorkingSellersService {

    private SellerRepository sellerRepository;

    public Response process(@NonNull Request request) {

        final List<Seller> sellers = sellerRepository.findByCourierId(request.getCourierId());
        final List<SellerDto> sellerDtos = Optional.ofNullable(sellers)
                .map(list -> list.stream()
                        .map(seller -> SellerDto.builder()
                                .id(seller.getId())
                                .name(seller.getName())
                                .phone(seller.getPhone())
                                .address(seller.getAddress())
                                .geoLat(seller.getGeoLat())
                                .geoLong(seller.getGeoLong())
                                .totalRts(seller.getTotalRts())
                                .build())
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());

        return Response.builder()
                .sellers(sellerDtos)
                .build();
    }

    @Builder
    @Getter
    @ToString
    public static class Request {

        @NonNull
        private Long courierId;

    }

    @Builder
    @Getter
    @ToString
    public static class Response {

        private List<SellerDto> sellers;

    }

}
