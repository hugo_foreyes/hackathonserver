package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.dto.SellerDto;
import com.alibaba.hackathon.freepickup.model.Seller;
import com.alibaba.hackathon.freepickup.model.SellerRepository;
import lombok.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GetNearestSellersService {

    private static final Double SM_TO_KM = 1.609344;

    private SellerRepository sellerRepository;

    @Transactional
    public Response process(@NonNull Request request) {

        List<SellerRepository.NearestSeller> nearestSellers;
        if (request.getNodeGeoLat() != null && request.getNodeGeoLong() != null) {
            nearestSellers = sellerRepository.findNearestSellersWithNode(
                    request.getGeoLat(),
                    request.getGeoLong(),
                    request.getNodeGeoLat(),
                    request.getNodeGeoLong(),
                    request.getLimit() == null ? 3 : request.getLimit());
        } else {
            nearestSellers = sellerRepository.findNearestSellers(
                    request.getGeoLat(),
                    request.getGeoLong(),
                    request.getLimit() == null ? 3 : request.getLimit());
        }
//        final List<Long> nearestIds = Optional.ofNullable(nearestSellers)
//                .map(list -> list.stream()
//                        .map(seller -> seller.getId())
//                        .collect(Collectors.toList()))
//                .orElse(Collections.emptyList());
//        if (CollectionUtils.isEmpty(nearestIds)) {
//            return Response.builder()
//                    .build();
//        }

        final Map<Long, SellerRepository.NearestSeller> nearestSellerMap = Optional.ofNullable(nearestSellers)
                .map(list -> list.stream()
                        .collect(Collectors.toMap(SellerRepository.NearestSeller::getId, Function.identity())))
                .orElse(Collections.emptyMap());
        if (CollectionUtils.isEmpty(nearestSellerMap)) {
            return Response.builder()
                    .build();
        }

        final List<Seller> sellers = sellerRepository.findAllById(nearestSellerMap.keySet());
        final List<SellerDto> sellerDtos = Optional.ofNullable(sellers)
                .map(list -> list.stream()
                        .map(seller -> SellerDto.builder()
                                .id(seller.getId())
                                .name(seller.getName())
                                .phone(seller.getPhone())
                                .address(seller.getAddress())
                                .geoLat(seller.getGeoLat())
                                .geoLong(seller.getGeoLong())
                                .totalRts(seller.getTotalRts())
                                .distance((float) (nearestSellerMap.get(seller.getId()).getDistance() * SM_TO_KM))
                                .build())
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());

        return Response.builder()
                .sellers(sellerDtos)
                .build();
    }

    @Builder
    @Getter
    @ToString
    public static class Request {

        @NonNull
        private Float geoLat;
        @NonNull
        private Float geoLong;

        private Float nodeGeoLat;
        private Float nodeGeoLong;

        private Long limit;

    }

    @Builder
    @Getter
    @ToString
    public static class Response {

        private List<SellerDto> sellers;

    }

}
