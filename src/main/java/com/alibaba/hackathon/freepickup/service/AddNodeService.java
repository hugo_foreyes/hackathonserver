package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.dto.NodeDto;
import com.alibaba.hackathon.freepickup.model.Node;
import com.alibaba.hackathon.freepickup.model.NodeRepository;
import com.alibaba.hackathon.freepickup.model.NodeType;
import lombok.*;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AddNodeService {

    private NodeRepository nodeRepository;

    public Response process(@NonNull Request request) {

        final NodeDto nodeDto = request.getNode();

        final Node saved = nodeRepository.save(Node.builder()
                .name(nodeDto.getName())
                .type(NodeType.PICKUP)
                .phone(nodeDto.getPhone())
                .address(nodeDto.getAddress())
                .geoLat(nodeDto.getGeoLat())
                .geoLong(nodeDto.getGeoLong())
                .cutOffTime(nodeDto.getCutOffTime())
                .build());

        return Response.builder()
                .node(NodeDto.builder()
                        .id(saved.getId())
                        .name(saved.getName())
                        .type(saved.getType().name())
                        .phone(saved.getPhone())
                        .address(saved.getAddress())
                        .geoLat(saved.getGeoLat())
                        .geoLong(saved.getGeoLong())
                        .cutOffTime(saved.getCutOffTime())
                        .build())
                .build();
    }

    @Builder
    @Getter
    @ToString
    public static class Request {

        @NonNull
        private NodeDto node;

    }

    @Builder
    @Getter
    @ToString
    public static class Response {

        private NodeDto node;

    }

}
