package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.dto.CourierDto;
import com.alibaba.hackathon.freepickup.dto.GetCourierByPhoneRequest;
import com.alibaba.hackathon.freepickup.model.Courier;
import com.alibaba.hackathon.freepickup.model.CourierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetCourierByPhoneService {

    private final CourierRepository courierRepository;

    public CourierDto process(GetCourierByPhoneRequest request) {
        Courier courier = courierRepository.findCourierByPhone(request.getPhone());
        CourierDto response = new CourierDto();

        BeanUtils.copyProperties(courier, response);

        return response;
    }

}
