package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.dto.SellerDto;
import com.alibaba.hackathon.freepickup.model.Seller;
import com.alibaba.hackathon.freepickup.model.SellerRepository;
import lombok.*;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AddSellerService {

    private SellerRepository sellerRepository;

    public Response process(@NonNull Request request) {

        final SellerDto sellerDto = request.getSeller();

        final Seller saved = sellerRepository.save(Seller.builder()
                .name(sellerDto.getName())
                .phone(sellerDto.getPhone())
                .address(sellerDto.getAddress())
                .geoLat(sellerDto.getGeoLat())
                .geoLong(sellerDto.getGeoLong())
                .totalRts(sellerDto.getTotalRts())
                .build());

        return Response.builder()
                .seller(SellerDto.builder()
                        .id(saved.getId())
                        .name(saved.getName())
                        .phone(saved.getPhone())
                        .address(saved.getAddress())
                        .geoLat(saved.getGeoLat())
                        .geoLong(saved.getGeoLong())
                        .totalRts(saved.getTotalRts())
                        .build())
                .build();
    }

    @Builder
    @Getter
    @ToString
    public static class Request {

        @NonNull
        private SellerDto seller;

    }

    @Builder
    @Getter
    @ToString
    public static class Response {

        private SellerDto seller;

    }

}
