package com.alibaba.hackathon.freepickup.service;


import com.alibaba.hackathon.freepickup.model.Courier;
import com.alibaba.hackathon.freepickup.model.CourierRepository;
import com.alibaba.hackathon.freepickup.model.CourierStatus;
import com.alibaba.hackathon.freepickup.model.Seller;
import com.alibaba.hackathon.freepickup.model.SellerRepository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.stereotype.Service;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Service
@AllArgsConstructor
public class FinishPickupAtStopService {
	private CourierRepository courierRepository;
	private SellerRepository sellerRepository;

	public Response process(@NonNull Request request) {
		Courier courier = this.courierRepository.findById(request.getCourierId())
			.orElseThrow(() -> new RuntimeException("Courier does not exist with id"+request.getCourierId()));

		if (request.isFree()) {
			courier.setStatus(CourierStatus.FREE.name());
			this.courierRepository.save(courier);
		}

		Seller seller = this.sellerRepository.findById(request.getSellerId())
			.orElseThrow(() -> new RuntimeException("Seller does not exist with id"+request.getSellerId()));

		if (!request.isSkip()) {
			seller.setTotalRts(0L);
		}
		seller.setCourierId(null);
		this.sellerRepository.save(seller);

		return Response.builder()
			.code(200L)
			.isSuccess(true)
			.data(true)
			.build();
	}

	@Builder
	@Getter
	@ToString
	public static class Request {

		@NonNull
		private Long courierId;
		@NonNull
		private Long sellerId;
		private boolean isFree;
		private boolean isSkip;

	}

	@Builder
	@Getter
	@ToString
	public static class Response {
		private Long code;
		private boolean isSuccess;
		private boolean data;
	}
}
