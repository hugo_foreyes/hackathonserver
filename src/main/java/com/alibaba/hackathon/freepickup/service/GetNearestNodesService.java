package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.dto.NodeDto;
import com.alibaba.hackathon.freepickup.model.Node;
import com.alibaba.hackathon.freepickup.model.NodeRepository;
import lombok.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GetNearestNodesService {

    private static final Double SM_TO_KM = 1.609344;

    private NodeRepository nodeRepository;

    @Transactional
    public Response process(@NonNull Request request) {

        List<NodeRepository.NearestNode> nearestNodes = nodeRepository.findNearestNodes(
                request.getGeoLat(),
                request.getGeoLong(),
                request.getLimit() == null ? 3 : request.getLimit());
//        final List<Long> nearestIds = Optional.ofNullable(nearestNodes)
//                .map(list -> list.stream()
//                        .map(seller -> seller.getId())
//                        .collect(Collectors.toList()))
//                .orElse(Collections.emptyList());
//        if (CollectionUtils.isEmpty(nearestIds)) {
//            return Response.builder()
//                    .build();
//        }
        final Map<Long, NodeRepository.NearestNode> nearestNodeMap = Optional.ofNullable(nearestNodes)
                .map(list -> list.stream()
                        .collect(Collectors.toMap(NodeRepository.NearestNode::getId, Function.identity())))
                .orElse(Collections.emptyMap());
        if (CollectionUtils.isEmpty(nearestNodeMap)) {
            return Response.builder()
                    .build();
        }

        final List<Node> nodes = nodeRepository.findAllById(nearestNodeMap.keySet());
        final List<NodeDto> nodeDtos = Optional.ofNullable(nodes)
                .map(list -> list.stream()
                        .map(node -> NodeDto.builder()
                                .id(node.getId())
                                .name(node.getName())
                                .type(node.getType().name())
                                .phone(node.getPhone())
                                .address(node.getAddress())
                                .geoLat(node.getGeoLat())
                                .geoLong(node.getGeoLong())
                                .cutOffTime(node.getCutOffTime())
                                .distance((float) (nearestNodeMap.get(node.getId()).getDistance() * SM_TO_KM))
                                .build())
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());

        return Response.builder()
                .nodes(nodeDtos)
                .build();
    }

    @Builder
    @Getter
    @ToString
    public static class Request {

        @NonNull
        private Float geoLat;
        @NonNull
        private Float geoLong;

        private Long limit;

    }

    @Builder
    @Getter
    @ToString
    public static class Response {

        private List<NodeDto> nodes;

    }

}
