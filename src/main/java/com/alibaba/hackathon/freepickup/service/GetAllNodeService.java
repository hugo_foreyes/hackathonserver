package com.alibaba.hackathon.freepickup.service;

import com.alibaba.hackathon.freepickup.dto.NodeDto;
import com.alibaba.hackathon.freepickup.model.Node;
import com.alibaba.hackathon.freepickup.model.NodeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Service
@AllArgsConstructor
public class GetAllNodeService {
	private NodeRepository nodeRepository;

	public List<NodeDto> process() {
		List<Node> nodes = this.nodeRepository.findAll();
		final List<NodeDto> nodeDtos = Optional.ofNullable(nodes)
			.map(list -> list.stream()
				.map(seller -> NodeDto.builder()
					.id(seller.getId())
					.name(seller.getName())
					.type(seller.getType().name())
					.phone(seller.getPhone())
					.address(seller.getAddress())
					.geoLat(seller.getGeoLat())
					.geoLong(seller.getGeoLong())
					.cutOffTime(seller.getCutOffTime())
					.build())
				.collect(Collectors.toList()))
			.orElse(Collections.emptyList());

		return nodeDtos;
	}

}
