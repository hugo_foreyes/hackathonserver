package com.alibaba.hackathon.freepickup.facade;

import com.alibaba.hackathon.freepickup.dto.GetSellersByLocationRequest;
import com.alibaba.hackathon.freepickup.dto.NodeDto;
import com.alibaba.hackathon.freepickup.service.AddNodeService;
import com.alibaba.hackathon.freepickup.service.GetAllNodeService;
import com.alibaba.hackathon.freepickup.service.GetNearestNodesService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@RestController
@AllArgsConstructor
@CrossOrigin
public class NodeController {

    private GetAllNodeService getAllNodeService;
    private AddNodeService addNodeService;
    private GetNearestNodesService getNearestNodesService;

    @GetMapping(path = "api/nodes/list")
    public GenericResponse<List<NodeDto>> getAllNodes() {
        return GenericResponse.successResponse(getAllNodeService.process());
    }

    @GetMapping(path = "api/nodes/test1")
    public GenericResponse<NodeDto> test1() {

        final String name = UUID.randomUUID().toString();
        final AddNodeService.Response serviceResponse = addNodeService.process(AddNodeService.Request.builder()
                .node(NodeDto.builder()
                        .name(name)
                        .type("PICKUP")
                        .phone(name)
                        .address(name)
                        .geoLat(new Random().nextFloat())
                        .geoLong(new Random().nextFloat())
                        .cutOffTime(name)
                        .build())
                .build());

        return GenericResponse.successResponse(serviceResponse.getNode());
    }

    @GetMapping(path = "api/nodes/test2")
    public GenericResponse<List<NodeDto>> test2() {

        final GetNearestNodesService.Response serviceResponse = getNearestNodesService.process(GetNearestNodesService.Request.builder()
                .geoLat(1f)
                .geoLong(2f)
                .build());

        return GenericResponse.successResponse(serviceResponse.getNodes());
    }

    @PostMapping(path = "/api/nodes/by-location", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    GenericResponse<List<NodeDto>> getNodesByLocation(@RequestBody GetSellersByLocationRequest request) {

        final GetNearestNodesService.Response serviceResponse = getNearestNodesService.process(GetNearestNodesService.Request.builder()
                .geoLat(request.getGeoLat())
                .geoLong(request.getGeoLong())
                .build());

        return GenericResponse.successResponse(serviceResponse.getNodes());
    }

}
