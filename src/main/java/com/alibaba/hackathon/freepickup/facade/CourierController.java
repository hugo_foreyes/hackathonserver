package com.alibaba.hackathon.freepickup.facade;

import com.alibaba.hackathon.freepickup.dto.*;
import com.alibaba.hackathon.freepickup.service.ChangeCourierDestinationNodeService;
import com.alibaba.hackathon.freepickup.service.ChangeCourierStatusService;
import com.alibaba.hackathon.freepickup.service.FinishPickupAtStopService;
import com.alibaba.hackathon.freepickup.service.FinishPickupAtStopService.Request;
import com.alibaba.hackathon.freepickup.service.FinishPickupAtStopService.Response;
import com.alibaba.hackathon.freepickup.service.GetCourierByPhoneService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@RestController
@AllArgsConstructor
@CrossOrigin
public class CourierController {
	private FinishPickupAtStopService finishPickupAtStopService;
	private ChangeCourierStatusService changeCourierStatusService;
	private GetCourierByPhoneService getCourierByPhoneService;
	private ChangeCourierDestinationNodeService changeCourierDestinationNodeService;

	@PostMapping(path = "api/couriers/finish-pick-up-at-stop", consumes = "application/json", produces = "application/json")
	public @ResponseBody Response finishPickupAtStop(@RequestBody FinishPickupAtStopRequest request) {
		return this.finishPickupAtStopService.process(Request.builder()
			.courierId(request.getCourierId())
			.sellerId(request.getSellerId())
			.isFree(request.isFree())
			.isSkip(request.isSkip())
			.build());
	}

	@PostMapping(path = "api/couriers/change-status", consumes = "application/json", produces = "application/json")
	public @ResponseBody ChangeCourierStatusService.Response changeStatus(@RequestBody ChangeCourierStatusRequest request) {
		return this.changeCourierStatusService.process(ChangeCourierStatusService.Request.builder()
			.status(request.getStatus())
			.courierId(request.getCourierId())
			.build());
	}

	@PostMapping(path = "api/couriers/info-by-phone", consumes = "application/json", produces = "application/json")
	public GenericResponse<CourierDto> getCourierInfoByPhone(@RequestBody GetCourierByPhoneRequest request) {
		return GenericResponse.successResponse(this.getCourierByPhoneService.process(request));
	}

	@PostMapping(path = "api/couriers/change-destination-node", consumes = "application/json", produces = "application/json")
	public GenericResponse<CourierDto> changeDestinationNode(@RequestBody ChangeCourierDestinationNodeRequest request) {
		return GenericResponse.successResponse(this.changeCourierDestinationNodeService.process(request));
	}

}
