package com.alibaba.hackathon.freepickup.facade;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class GenericResponse<T> {

    private Long code;
    private boolean isSuccess;
    private T data;

    public static <T> GenericResponse successResponse(T t) {
        return GenericResponse.<T>builder()
                .code(200l)
                .isSuccess(true)
                .data(t)
                .build();
    }

}
