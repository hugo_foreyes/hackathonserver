package com.alibaba.hackathon.freepickup.facade;

import com.alibaba.hackathon.freepickup.dto.GetSellersByCourierIdRequest;
import com.alibaba.hackathon.freepickup.dto.GetSellersByLocationRequest;
import com.alibaba.hackathon.freepickup.dto.SellerDto;
import com.alibaba.hackathon.freepickup.dto.SetSellersForCourierRequest;
import com.alibaba.hackathon.freepickup.service.*;
import com.alibaba.hackathon.freepickup.service.GenerateRtsForSellerService.Response;
import com.alibaba.hackathon.freepickup.service.SetListOfSellersForCourierService.Request;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@RestController
@AllArgsConstructor
@CrossOrigin
public class SellerController {

    private AddSellerService addSellerService;
    private GetNearestSellersService getNearestSellersService;
    private GetCurrentWorkingSellersService getCurrentWorkingSellersService;
    private GenerateRtsForSellerService generateRtsForSellerService;
    private SetListOfSellersForCourierService setListOfSellersForCourierService;

    @GetMapping(path = "api/sellers/generate-rts")
    public Response generateRts() {
        return this.generateRtsForSellerService.process();
    }

    @GetMapping(path = "api/sellers/test1")
    public GenericResponse<SellerDto> test1() {

        final String name = UUID.randomUUID().toString();
        final AddSellerService.Response serviceResponse = addSellerService.process(AddSellerService.Request.builder()
                .seller(SellerDto.builder()
                        .name(name)
                        .phone(name)
                        .address(name)
                        .geoLat(new Random().nextFloat())
                        .geoLong(new Random().nextFloat())
                        .totalRts(System.currentTimeMillis() % 2 == 0 ? null : 10L)
                        .build())
                .build());

        return GenericResponse.successResponse(serviceResponse.getSeller());
    }

    @GetMapping(path = "api/sellers/test2")
    public GenericResponse<List<SellerDto>> test2() {

        final long timeMillis = System.currentTimeMillis();
        final GetNearestSellersService.Response serviceResponse = getNearestSellersService.process(GetNearestSellersService.Request.builder()
                .geoLat(1f)
                .geoLong(2f)
                .nodeGeoLat(timeMillis % 2 == 0 ? null : 3f)
                .nodeGeoLong(timeMillis % 2 == 0 ? null : 3f)
                .build());

        return GenericResponse.successResponse(serviceResponse.getSellers());
    }

    @GetMapping(path = "api/sellers/test3")
    public GenericResponse<List<SellerDto>> test3() {

        final GetCurrentWorkingSellersService.Response serviceResponse = getCurrentWorkingSellersService.process(GetCurrentWorkingSellersService.Request.builder()
                .courierId(1l)
                .build());

        return GenericResponse.successResponse(serviceResponse.getSellers());
    }

    @PostMapping(path = "/api/sellers/by-location", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    GenericResponse<List<SellerDto>> getSellersByLocation(@RequestBody GetSellersByLocationRequest request) {

        final GetNearestSellersService.Response serviceResponse = getNearestSellersService.process(GetNearestSellersService.Request.builder()
                .geoLat(request.getGeoLat())
                .geoLong(request.getGeoLong())
                .build());

        return GenericResponse.successResponse(serviceResponse.getSellers());
    }

    @PostMapping(path = "/api/sellers/set-sellers-for-courier", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    SetListOfSellersForCourierService.Response setSellersForCourier(@RequestBody SetSellersForCourierRequest request) {
        return this.setListOfSellersForCourierService.process(Request.builder()
            .courierId(request.getCourierId())
            .sellerIds(request.getSellerIds())
            .build());
    }

    @PostMapping(path = "/api/sellers/by-courier-id", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    GenericResponse<List<SellerDto>> getSellersByCourierId(@RequestBody GetSellersByCourierIdRequest request) {

        final GetCurrentWorkingSellersService.Response serviceResponse = getCurrentWorkingSellersService.process(GetCurrentWorkingSellersService.Request.builder()
                .courierId(request.getCourierId())
                .build());

        return GenericResponse.successResponse(serviceResponse.getSellers());
    }

}
