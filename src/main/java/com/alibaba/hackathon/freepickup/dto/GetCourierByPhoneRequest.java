package com.alibaba.hackathon.freepickup.dto;

import lombok.Data;

@Data
public class GetCourierByPhoneRequest {

    private String phone;

}
