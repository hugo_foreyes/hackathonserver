package com.alibaba.hackathon.freepickup.dto;

import lombok.Data;

@Data
public class ChangeCourierDestinationNodeRequest {

    private Long courierId;
    private Long destinationNodeId;

}
