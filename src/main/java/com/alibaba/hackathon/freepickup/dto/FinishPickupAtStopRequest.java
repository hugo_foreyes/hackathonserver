package com.alibaba.hackathon.freepickup.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NonNull;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Data
public class FinishPickupAtStopRequest {
	@NonNull
	private Long courierId;
	@NonNull
	private Long sellerId;
	@JsonProperty
	private boolean isFree;
	@JsonProperty
	private boolean isSkip;
}
