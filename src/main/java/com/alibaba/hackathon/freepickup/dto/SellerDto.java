package com.alibaba.hackathon.freepickup.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Builder
@Getter
@ToString
public class SellerDto {

    private Long id;
    @NonNull
    private String name;
    private String phone;
    private String address;
    @NonNull
    private Float geoLat;
    @NonNull
    private Float geoLong;
    private Long totalRts;

    private Float distance;

}
