package com.alibaba.hackathon.freepickup.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourierDto {

    private Long id;
    private String name;
    private String status;
    private Long destinationNodeId;

}
