package com.alibaba.hackathon.freepickup.dto;

import lombok.Data;

@Data
public class GetSellersByLocationRequest {

    private Float geoLat;
    private Float geoLong;

}
