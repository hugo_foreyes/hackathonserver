package com.alibaba.hackathon.freepickup.dto;

import lombok.Data;

@Data
public class GetSellersByCourierIdRequest {

    private Long courierId;

}
