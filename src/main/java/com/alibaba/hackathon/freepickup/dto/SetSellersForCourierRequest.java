package com.alibaba.hackathon.freepickup.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.util.List;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class SetSellersForCourierRequest {
	@NonNull
	@Positive
	private Long courierId;
	@NotEmpty
	@JsonProperty
	private List<Long> sellerIds;
}
