package com.alibaba.hackathon.freepickup.dto;

import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.Positive;

/**
 * @author lazada team
 * @date 2022/02/23
 */
@Data
public class ChangeCourierStatusRequest {
	@NonNull
	@Positive
	private Long courierId;
	@NonNull
	private String status;
}
